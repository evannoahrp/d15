// Objectives

//     Mampu menggunakan built in function pada Array seperti .push, .shift, .unshift, dll
//     Mampu membuat function dan mengerti penggunaan parameter dari sebuah function
//     Mampu menggunakan template literals

// RESTRICTION Hanya boleh menggunakan built-in function untuk menambahkan atau mengurangi data dalam array, seperti .shift(), unShift(), push(), dan pop()
// Directions
// Antrian

// Diberikan sebuah function ganjilGenap yang menerima satu parameter plat bertipe string. Parameter plat berisi informasi kumpulan plat dimana nomor antar plat dipisahkan oleh titik koma(;).

// Function ini akan mengembalikan keterangan jumlah plat genap dan jumlah plat ganjil.

function ganjilGenap(plat) {
  try {
    if (plat != "") {
      let hasil = plat.split(";");
      // console.log(hasil);
      var genap = 0;
      var ganjil = 0;
      hasil.forEach((element) => {
        if (element % 2 == 0) {
          genap++;
        } else {
          ganjil++;
        }
      });
      if (genap != 0) {
        if (ganjil != 0) {
          console.log(`plat genap sebanyak ${genap} dan plat ganjil sebanyak ${ganjil}`);
        } else {
          console.log(`plat genap sebanyak ${genap} dan plat ganjil tidak ditemukan`);
        }
      } else {
        console.log(`plat ganjil sebanyak ${ganjil} dan plat genap tidak ditemukan`);
      }
    } else {
      console.log("plat tidak ditemukan");
    }
  } catch {
    console.log("invalid data");
  }
}

console.log(ganjilGenap("2341;3429;864;1309;1276")); //plat genap sebanyak 2 dan plat ganjil sebanyak 3
console.log(ganjilGenap("2347;3429;1305")); //plat ganjil sebanyak 3 dan plat genap tidak ditemukan
console.log(ganjilGenap("864;1308;1276;1432")); //plat genap sebanyak 4 dan plat ganjil tidak ditemukan
console.log(ganjilGenap("")); //plat tidak ditemukan
console.log(ganjilGenap()); //invalid data
